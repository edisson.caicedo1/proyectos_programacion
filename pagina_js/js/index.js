//Pedir al usuario que ingrese el número
let input = parseInt(prompt("Digite el número entero para crear la tabla: "));

//Introducir los encabezados de la tabla
let resultado = "<tr><th>Multiplicación </th> <th>Resultado</th></tr>";

//Ciclo o bucle para que repita una acción. Multiplicar 10 veces el número
for(i=1;i<=10;i++)
    {
        resultado += '<tr><td>'+input + ' X '+ i + ' = ' + ' </td> <td>'+(input*i)+ '</td></tr>'
    }

//Mostrar en pantalla el resultado
document.getElementById('tabla').innerHTML =resultado;